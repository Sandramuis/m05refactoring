/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 6. Preparant el terreny: ara seriosament, proves unitàries
 * Com que estem d’entrenament, dedica una mica més d’esforç i crea un joc de 
 * prova amb cobertura 100% de les línies del mètode Client.informe()
 * Pista: cap lloguer, un lloguer, més d’un lloguer, lloguer de vehicle general
 *  amb un dia, lloguer de general amb 2 dies, etc.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
