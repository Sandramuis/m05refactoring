/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 6. Preparant el terreny: ara seriosament, proves unitàries
 * Com que estem d’entrenament, dedica una mica més d’esforç i crea un joc de 
 * prova amb cobertura 100% de les línies del mètode Client.informe()
 * Pista: cap lloguer, un lloguer, més d’un lloguer, lloguer de vehicle general
 *  amb un dia, lloguer de general amb 2 dies, etc.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

}
