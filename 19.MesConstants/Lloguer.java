/*Sandra Muñoz Isidro
 * M05UF2 
 * Exercici 19. Més constants
 * Considera ara el fragment de codi que pertany al mètode Lloguer.quantitat().
 * Signifiquen el mateix els tres cops que hi apareix el valor 3?
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;
	public Lloguer lloguer;
    private static final int BASIC_PREU_STANDARD = 3;
    private static final int BASIC_DIES_CANVI_TARIFA = 3;
    private static final double BASIC_PREU_REDUIT = 1.5;
    
    private static final int GENERAL_DIES_CANVI_TARIFA = 2;
    private static final int GENERAL_PREU_STANDARD = 4;
    private static final double GENERAL_PREU_REDUIT = 2.5;
    private static final int LUXE_PREU_STANDARD = 6;

	public Lloguer(java.util.Date date, int dies, Vehicle vehicle) {
		this.data = date;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public double quantitat() {
		double totalPagar = 0;
        switch (this.getVehicle().getCategoria()) {
            case Vehicle.BASIC:
                totalPagar += BASIC_PREU_STANDARD;
                if (this.getDies() > BASIC_DIES_CANVI_TARIFA) {
                    totalPagar += (this.getDies() - BASIC_DIES_CANVI_TARIFA) * BASIC_PREU_REDUIT;
                }
                break;
            case Vehicle.GENERAL:
                totalPagar += GENERAL_PREU_STANDARD;
                if (this.getDies() > GENERAL_DIES_CANVI_TARIFA) {
                    totalPagar += (this.getDies() - GENERAL_DIES_CANVI_TARIFA) * GENERAL_PREU_REDUIT;
                }
                break;
            case Vehicle.LUXE:
                totalPagar += this.getDies() * 6;
                break;
        }
        return totalPagar;

	}

	public int bonificacions() {
		if (getVehicle().getCategoria() == Vehicle.LUXE && getDies() > 1) {
			return 2;
		} else
			return 1;
	}
	

	public Date getData() {
		return (Date) data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}
}
