/*Sandra Muñoz Isidro
 * M05UF2 
 * Exercici 19. Més constants
 * Considera ara el fragment de codi que pertany al mètode Lloguer.quantitat().
 * Signifiquen el mateix els tres cops que hi apareix el valor 3?
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
