/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 8. quantitatPerLloguer() a la classe Lloguer
 * Mou el mètode Client.quantitatPerLloguer() a Lloguer.quantitat(). 
 * Com sempre, assegura’t que els canvis no han trencat res (passa els jocs de prova)
 * ara aquest mètode caldrà que accessible des d’una altra classe (visibilitat)
 * com que el mètode ara és de Lloguer, el seu nom antic és redundant. 
 * Per això es proposa reanomenar-lo a quantitat().
 * ara ja no li caldrà rebre el lloguer com a paràmetre. Saps perquè? 
 * La crida des de Client.informe() serà una mica diferent.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}
	
	public double quantitat() {
		double quantitat = 0;
		switch (vehicle.getCategoria()) {
			case Vehicle.BASIC:
				quantitat += 3;
				if (dies > 3) {
					quantitat += (dies - 3) * 1.5;
				}
				break;
			case Vehicle.GENERAL:
				quantitat += 4;
				if (getDies() > 2) {
					quantitat += (dies - 2) * 2.5;
				}
				break;
			case Vehicle.LUXE:
				quantitat += dies * 6;
				break;
			}
		return quantitat;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

}
