/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 8. quantitatPerLloguer() a la classe Lloguer
 * Mou el mètode Client.quantitatPerLloguer() a Lloguer.quantitat(). 
 * Com sempre, assegura’t que els canvis no han trencat res (passa els jocs de prova)
 * ara aquest mètode caldrà que accessible des d’una altra classe (visibilitat)
 * com que el mètode ara és de Lloguer, el seu nom antic és redundant. 
 * Per això es proposa reanomenar-lo a quantitat().
 * ara ja no li caldrà rebre el lloguer com a paràmetre. Saps perquè? 
 * La crida des de Client.informe() serà una mica diferent.
 * */
import static org.junit.Assert.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class TestInforme {
	SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
	private Date date = null;

	private Client creaClientSenseLloguers() {
		return new Client("48484785L", "Sandra", "698787878");
	}

	@Test
	public void testInforme() throws ParseException {

		Client client = new Client("48484785L", "Sandra", "698787878");
		Vehicle vehicleGeneral = new Vehicle("MarcaGeneral", "ModelGeneral",
				Vehicle.GENERAL);

		Lloguer lloguerGeneral = new Lloguer(dateFormat.parse("3/8/2014"), 2,
				vehicleGeneral);

		client.afegeix(lloguerGeneral);

	}

	@Test
	public void testInformeSenseLloguers() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "666666666");
		String str = "Informe de lloguers del client Sandra (12345678S)\nImport a pagar: 0.0€\nPunts guanyats: 0\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleBasicFinsATresDies()
			throws ParseException {

		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.BASIC);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 90.0€\nImport a pagar: 90.0€\nPunts guanyats: 1\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleBasicAmbMesDeTresDies()
			throws ParseException {

		Client betatester = creaClientSenseLloguers();
		Vehicle vehicleBasic = new Vehicle("ModelBasic", "MarcaBasic",
				Vehicle.BASIC);
		Lloguer lloguerBasic = new Lloguer(dateFormat.parse("2/7/2013"), 4,
				vehicleBasic);
		betatester.afegeix(lloguerBasic);
		String str = "Informe de lloguers del client Sandra (48484785L)\n\tModelBasic MarcaBasic: 135.0€\nImport a pagar: 135.0€\nPunts guanyats: 1\n";
		assertEquals(str, betatester.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleGeneralDeDosDies() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 2, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 120.0€\nImport a pagar: 120.0€\nPunts guanyats: 1\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testMesDunLloguerDeTresDiesGeneral() {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		Lloguer lloguer2 = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer2);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 195.0€\n\tVista Tata: 195.0€\nImport a pagar: 390.0€\nPunts guanyats: 2\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerVehicleGeneral_1dia() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 1, vehicle);
		sandra.afegeix(lloguer);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 120.0€\nImport a pagar: 120.0€\nPunts guanyats: 1\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testMesDunLloguerDeUnDiaLuxe() {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.LUXE);
		Lloguer lloguer = new Lloguer(date, 1, vehicle);
		Lloguer lloguer2 = new Lloguer(date, 1, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer2);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 180.0€\n\tVista Tata: 180.0€\nImport a pagar: 360.0€\nPunts guanyats: 2\n";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerVehicleLuxe_varis_dies() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.LUXE);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		String str = "Informe de lloguers del client Sandra (12345678S)\n\tVista Tata: 540.0€\nImport a pagar: 540.0€\nPunts guanyats: 2\n";
		assertEquals(str, sandra.informe());
	}

}