/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 8. quantitatPerLloguer() a la classe Lloguer
 * Mou el mètode Client.quantitatPerLloguer() a Lloguer.quantitat(). 
 * Com sempre, assegura’t que els canvis no han trencat res (passa els jocs de prova)
 * ara aquest mètode caldrà que accessible des d’una altra classe (visibilitat)
 * com que el mètode ara és de Lloguer, el seu nom antic és redundant. 
 * Per això es proposa reanomenar-lo a quantitat().
 * ara ja no li caldrà rebre el lloguer com a paràmetre. Saps perquè? 
 * La crida des de Client.informe() serà una mica diferent.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
