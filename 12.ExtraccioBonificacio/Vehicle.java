/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 12. Extracció del càlcul de bonificacions
 * De la mateixa manera que hem extret de Client.informe() 
 * el càlcul de l’import de cada lloguer, podem plantejar-nos extreure el 
 * càlcul de les bonificacions.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
