/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 12. Extracció del càlcul de bonificacions
 * De la mateixa manera que hem extret de Client.informe() 
 * el càlcul de l’import de cada lloguer, podem plantejar-nos extreure el 
 * càlcul de les bonificacions.
 * */
import java.util.Vector;

public class Client {
	private String nif;
	private String nom;
	private String telefon;
	private Vector<Lloguer> lloguers;

	public Vector<Lloguer> getLloguers() {
		return lloguers;
	}

	public void setLloguers(Vector<Lloguer> lloguers) {
		this.lloguers = lloguers;
	}

	public Client(String nif, String nom, String telefon) {
		this.nif = nif;
		this.nom = nom;
		this.telefon = telefon;
		this.lloguers = new Vector<Lloguer>();
	}

	public String getNif() {
		return nif;
	}

	public String getNom() {
		return nom;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public void afegeix(Lloguer lloguer) {
		if (!lloguers.contains(lloguer)) {
			lloguers.add(lloguer);
		}
	}

	public void elimina(Lloguer lloguer) {
		if (lloguers.contains(lloguer)) {
			lloguers.remove(lloguer);
		}
	}

	public boolean conte(Lloguer lloguer) {
		return lloguers.contains(lloguer);
	}

	public String informe() {
		double total = 0;
		int bonificacions = 0;
		String resultat = "Informe de lloguers del client " + getNom() + " ("
				+ getNif() + ")\n";
		for (Lloguer lloguer : lloguers) {
			bonificacions += bonificacionsDeLloguer(lloguer);
			resultat += "\t" + lloguer.getVehicle().getMarca() + " "
					+ lloguer.getVehicle().getModel() + ": "
					+ (lloguer.quantitat() * 30) + "€" + "\n";
			total += lloguer.quantitat() * 30;
		}

		resultat += "Import a pagar: " + total + "€\n" + "Punts guanyats: "
				+ bonificacions + "\n";
		return resultat;
	}

	private int bonificacionsDeLloguer(Lloguer lloguer) {

		if (lloguer.getVehicle().getCategoria() == Vehicle.LUXE
				&& lloguer.getDies() > 1) {
			return 2;
		} else
			return 1;
	}

}