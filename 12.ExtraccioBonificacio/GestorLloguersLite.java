/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 12. Extracció del càlcul de bonificacions
 * De la mateixa manera que hem extret de Client.informe() 
 * el càlcul de l’import de cada lloguer, podem plantejar-nos extreure el 
 * càlcul de les bonificacions.
 * */
import java.text.SimpleDateFormat;
import java.util.Date;

public class GestorLloguersLite {
	static SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");

	public static String retornaInfo(Client client) {

		String strBase = "Client: %s\n\t%s\n\t%s\n" + "Lloguers: %d\n";
		String strLloguerBase = "\t%d. vehicle: %s %s\n"
				+ "\t\tdata d'inici: %s\n\t\tdies llogats: %d\n";

		String strClient = String.format(strBase, client.getNom(), client.getNif(),
				client.getTelefon(), client.getLloguers().size());

		int cont = 0;
		for (Lloguer lloguer : client.getLloguers()) {
			cont++;

			String strLloguer = String.format(strLloguerBase, cont,
					lloguer.getVehicle().getModel(), lloguer.getVehicle().getMarca(),
					dateFormat.format(lloguer.getData()), lloguer.getDies());
			strClient += strLloguer;
		}

		return strClient;

	}

	public static void main(String[] args) throws Throwable {
		Client client = new Client("48484785L", "Sandra", "698787878");

		// demostració de construcció de vehicles de totes les categories
		Vehicle vehicleBasic = new Vehicle("Tata", "Vista", Vehicle.BASIC);

		Vehicle vehicleGeneral = new Vehicle("MarcaGen", "ModelGen", Vehicle.GENERAL);

		Vehicle vehicleLuxe = new Vehicle("MarcaLux", "ModelLux", Vehicle.LUXE);

		// demostració de construccuó de lloguers amb data
		Lloguer lloguerBasic = new Lloguer(dateFormat.parse("2/8/2013"), 2, vehicleBasic);
		Lloguer lloguerGeneral = new Lloguer(dateFormat.parse("1/6/2014"), 2,
				vehicleGeneral);
		Lloguer lloguerLuxe = new Lloguer(dateFormat.parse("19/9/2016"), 2, vehicleLuxe);

		client.afegeix(lloguerBasic);
		client.afegeix(lloguerGeneral);
		client.afegeix(lloguerLuxe);

		System.out.println(retornaInfo(client));
	}

}
