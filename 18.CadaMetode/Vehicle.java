/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 18. Cada mètode fa una única cosa
 * Distribueix el mètode Client.informe() de manera que quedin separades les 
 * funcionalitats de construcció de la capçalera, el detall i el peu de l’informe,
 *  tal i com acabem de veure.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
