/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 13. Reassignant responsabilitats
 * Considera si el mètode bonificacionsDeLloguer que hem creat per l’exercici anterior,
 * correspon realment a la classe on es troba. Mou-lo allà on creguis oportú fent servir
 * moviment de mètode. El codi resultant hauria de permetre que Client.informe() 
 * funcioni.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
