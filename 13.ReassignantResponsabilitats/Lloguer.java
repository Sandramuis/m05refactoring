/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 13. Reassignant responsabilitats
 * Considera si el mètode bonificacionsDeLloguer que hem creat per l’exercici anterior,
 * correspon realment a la classe on es troba. Mou-lo allà on creguis oportú fent servir
 * moviment de mètode. El codi resultant hauria de permetre que Client.informe() 
 * funcioni.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public double quantitat() {
		double quantitat = 0;
		switch (vehicle.getCategoria()) {
		case Vehicle.BASIC:
			quantitat += 3;
			if (dies > 3) {
				quantitat += (dies - 3) * 1.5;
			}
			break;
		case Vehicle.GENERAL:
			quantitat += 4;
			if (getDies() > 2) {
				quantitat += (dies - 2) * 2.5;
			}
			break;
		case Vehicle.LUXE:
			quantitat += dies * 6;
			break;
		}
		return quantitat;
	}

	public int bonificacions() {
		if (getVehicle().getCategoria() == Vehicle.LUXE && getDies() > 1) {
			return 2;
		} else
			return 1;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

}
