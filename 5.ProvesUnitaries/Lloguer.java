/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 3. El Gestor de Lloguers lite
 * Codifica la classe GestorLloguersLite que crei un client amb 
 * el teu nom, que disposi de un mínim de tres lloguers, un per 
 * cada tipus de vehicle.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

}
