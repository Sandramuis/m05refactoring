
/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 2. Codificant el Gestor de Lloguers
 * A partir del diagrama anterior, fes una proposta de codificació de les 
 * classes que hi apareixen.
 * Assegura't que les propietats que apareixen al diagrama de classes disposen
 * d'accessors adequats. Considera el codi següent per la classe Client com a 
 * referència.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

}
