
/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 2. Codificant el Gestor de Lloguers
 * A partir del diagrama anterior, fes una proposta de codificació de les 
 * classes que hi apareixen.
 * Assegura't que les propietats que apareixen al diagrama de classes disposen
 * d'accessors adequats. Considera el codi següent per la classe Client com a 
 * referència.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;

	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
