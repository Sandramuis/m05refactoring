/*Sandra Muñoz Isidro
 * M05UF2Exercici 17. Eliminació de la variable bonificacions
 * Ara li toca el torn de la variable bonificacions. Elimina-la del codi de Client.informe().
 *  Amb la nova versió, aquest mètode tindrà el següent aspecte:
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
