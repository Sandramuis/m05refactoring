/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 7: Quantitat per lloguer
 * Implementa el mètode privat Client.quantitatPerLloguer() de manera que el codi 
 * resultant continuï passant els jocs de prova.
 * Atenció: no intentis fer massa. Simplement cal que moguis el bloc del switch a 
 * dins del nou mètode, declaris alguna variable on guardar l’import 
 * i retornis el seu valor al final.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
