/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 7: Quantitat per lloguer
 * Implementa el mètode privat Client.quantitatPerLloguer() de manera que el codi 
 * resultant continuï passant els jocs de prova.
 * Atenció: no intentis fer massa. Simplement cal que moguis el bloc del switch a 
 * dins del nou mètode, declaris alguna variable on guardar l’import 
 * i retornis el seu valor al final.
 * */
import java.util.Date;

public class Lloguer {
	private Date data;
	private int dies;
	private Vehicle vehicle;

	public Lloguer(Date data, int dies, Vehicle vehicle) {
		this.data = data;
		this.dies = dies;
		this.vehicle = vehicle;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getDies() {
		return dies;
	}

	public void setDies(int dies) {
		this.dies = dies;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

}
