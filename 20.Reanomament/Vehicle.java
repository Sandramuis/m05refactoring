/*Sandra Muñoz Isidro
 * M05UF2 
 * Exercici 20. Reanomenament
 * Ara que hem aprofundit més en els conceptes que envolten al càlcul de
 * l’import del lloguer, ens adonem que el mot quantitat no és representatiu
 * del que conté la variable quantitat al mètode Lloguer.quantitat().
 * De fet, el nom del mètode presenta el mateix problema!
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
