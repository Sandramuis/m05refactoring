/*Sandra Muñoz Isidro
 * M05UF2
 * Exercici 3. El Gestor de Lloguers lite
 * Codifica la classe GestorLloguersLite que crei un client amb 
 * el teu nom, que disposi de un mínim de tres lloguers, un per 
 * cada tipus de vehicle.
 * */
public class Vehicle {
	private String model;
	private String marca;
	private int categoria;
	public static final int BASIC = 0;
	public static final int GENERAL = 1;
	public static final int LUXE = 2;
	
	public Vehicle(String marca, String model, int categoria) {
		this.marca = marca;
		this.model = model;
		this.categoria = categoria;
	}

	public Vehicle getVehicle() {
		return this;
	}

	public String getModel() {
		return model;
	}

	public String getMarca() {
		return marca;
	}

	public int getCategoria() {
		return categoria;
	}

}
