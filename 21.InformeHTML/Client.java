/*Sandra Muñoz Isidro
 * M05UF2 
 * Exercici 21. L’informe en HTML 
 * Ara que Client.informe() només s’encarrega directament 
 * de composar l’informe, és el moment de fer la versió en HTML.
 * */
import java.util.Vector;

public class Client {
	private String nif;
	private String nom;
	private String telefon;
	private Vector<Lloguer> lloguers;
	private static final double EUROS_PER_UNITAT_DE_COST = 30;

	public Vector<Lloguer> getLloguers() {
		return lloguers;
	}

	public void setLloguers(Vector<Lloguer> lloguers) {
		this.lloguers = lloguers;
	}

	public Client(String nif, String nom, String telefon) {
		this.nif = nif;
		this.nom = nom;
		this.telefon = telefon;
		this.lloguers = new Vector<Lloguer>();
	}

	public String getNif() {
		return nif;
	}

	public String getNom() {
		return nom;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public void afegeix(Lloguer lloguer) {
		if (!lloguers.contains(lloguer)) {
			lloguers.add(lloguer);
		}
	}

	public void elimina(Lloguer lloguer) {
		if (lloguers.contains(lloguer)) {
			lloguers.remove(lloguer);
		}
	}

	public boolean conte(Lloguer lloguer) {
		return lloguers.contains(lloguer);
	}

	public String informe() {
		return composaCapsalera() + composaRestaInforme();
	}

	private String composaCapsalera() {
		String resultat = "<!DOCTYPE>" + "<meta charset='utf-8'>"
				 + "<h1>Informe Lloguers</h1>";
		resultat += "<p>Informe de lloguers del client <em>" + getNom()
				 + "</em> <strong>(" + getNif() + ")</strong>\n";
		String resultatlloguer = "<table border 1px>";
		resultat += resultatlloguer;
		resultat += "<tr>" +
				  "<td><strong>Marca</strong></td>" +
				  "<td><strong>Model</strong></td>" +
				  "<td><strong>Import</strong></td>"+
				  "</tr><tr>";
		for (Lloguer lloguer : lloguers) {
			resultat += "<tr><td><strong>" + lloguer.getVehicle().getMarca() +
					 "</strong></td>" +
					 "<td><strong>"+ lloguer.getVehicle().getModel() +"</strong></td>" +
					 "<td>" + (lloguer.costPerVehicle() * EUROS_PER_UNITAT_DE_COST)+"€</td></tr>";
		}
		resultat += "</table>";
		return resultat;

	}

	private String composaRestaInforme() {
		String resultat = "<p>Import a pagar: <em>" + importTotal()
				+ "€</em></p>" + "<p>Punts guanyats: <em>"
				+ bonificacionsTotals() + "</em></p>";
		return resultat;
	}

	private int bonificacionsTotals() {
		int bonificacions = 0;
		for (Lloguer lloguer : lloguers) {
			bonificacions += lloguer.bonificacions();
		}
		return bonificacions;
	}

	private double importTotal() {
		double total = 0;
		for (Lloguer lloguer : lloguers) {
			total += lloguer.costPerVehicle() * EUROS_PER_UNITAT_DE_COST;
		}
		return total;
	}

}