/*Sandra Muñoz Isidro
 * M05UF2 
 * Exercici 21. L’informe en HTML 
 * Ara que Client.informe() només s’encarrega directament 
 * de composar l’informe, és el moment de fer la versió en HTML.
 * */
import static org.junit.Assert.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class TestInforme {
	SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
	private Date date = null;

	private Client creaClientSenseLloguers() {
		return new Client("48484785L", "Sandra", "698787878");
	}

	@Test
	public void testInforme() throws ParseException {

		Client client = new Client("48484785L", "Sandra", "698787878");
		Vehicle vehicleGeneral = new Vehicle("MarcaGeneral", "ModelGeneral",
				Vehicle.GENERAL);

		Lloguer lloguerGeneral = new Lloguer(dateFormat.parse("3/8/2014"), 2,
				vehicleGeneral);

		client.afegeix(lloguerGeneral);

	}

	@Test
	public void testInformeSenseLloguers() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "666666666");
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr>" +
				"</table><p>Import a pagar: <em>0.0€</em></p>" +
				"<p>Punts guanyats: <em>0</em></p>";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleBasicFinsATresDies()
			throws ParseException {

		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.BASIC);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>90.0€</td></tr>" +
				"</table><p>Import a pagar: <em>90.0€</em></p>" +
				"<p>Punts guanyats: <em>1</em></p>";		
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleBasicAmbMesDeTresDies()
			throws ParseException {

		Client betatester = creaClientSenseLloguers();
		Vehicle vehicleBasic = new Vehicle("ModelBasic", "MarcaBasic",
				Vehicle.BASIC);
		Lloguer lloguerBasic = new Lloguer(dateFormat.parse("2/7/2013"), 4,
				vehicleBasic);
		betatester.afegeix(lloguerBasic);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(48484785L)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>ModelBasic</strong></td><td><strong>MarcaBasic</strong></td><td>135.0€</td></tr>" +			
				"</table><p>Import a pagar: <em>135.0€</em></p>" +
				"<p>Punts guanyats: <em>1</em></p>";			
		
		assertEquals(str, betatester.informe());
	}

	@Test
	public void testUnLloguerAmbVehicleGeneralDeDosDies() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 2, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>120.0€</td></tr>" +
				"</table><p>Import a pagar: <em>120.0€</em></p>" +
				"<p>Punts guanyats: <em>1</em></p>";
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testMesDunLloguerDeTresDiesGeneral() {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		Lloguer lloguer2 = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer2);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>195.0€</td></tr>" +
				"<tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>195.0€</td></tr>" +
				"</table><p>Import a pagar: <em>390.0€</em></p>" +
				"<p>Punts guanyats: <em>2</em></p>";	
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerVehicleGeneral_1dia() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.GENERAL);
		Lloguer lloguer = new Lloguer(date, 1, vehicle);
		sandra.afegeix(lloguer);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>120.0€</td></tr>" +			
				"</table><p>Import a pagar: <em>120.0€</em></p>" +
				"<p>Punts guanyats: <em>1</em></p>";			
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testMesDunLloguerDeUnDiaLuxe() {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.LUXE);
		Lloguer lloguer = new Lloguer(date, 1, vehicle);
		Lloguer lloguer2 = new Lloguer(date, 1, vehicle);
		sandra.afegeix(lloguer);
		sandra.afegeix(lloguer2);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>180.0€</td></tr>" +
				"<tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>180.0€</td></tr>" +
				"</table><p>Import a pagar: <em>360.0€</em></p>" +
				"<p>Punts guanyats: <em>2</em></p>";	
		assertEquals(str, sandra.informe());
	}

	@Test
	public void testUnLloguerVehicleLuxe_varis_dies() throws ParseException {
		Client sandra = new Client("12345678S", "Sandra", "66666666");
		Vehicle vehicle = new Vehicle("Vista", "Tata", Vehicle.LUXE);
		Lloguer lloguer = new Lloguer(date, 3, vehicle);
		sandra.afegeix(lloguer);
		String str = "<!DOCTYPE><meta charset='utf-8'><h1>Informe Lloguers</h1>" +
				"<p>Informe de lloguers del client <em>Sandra</em> <strong>(12345678S)</strong>\n" +
				"<table border 1px>" +
				"<tr><td><strong>Marca</strong></td>" +
				"<td><strong>Model</strong></td>" +
				"<td><strong>Import</strong></td></tr>" +
				"<tr><tr><td><strong>Vista</strong></td><td><strong>Tata</strong></td><td>540.0€</td></tr>" +
				"</table><p>Import a pagar: <em>540.0€</em></p>" +
				"<p>Punts guanyats: <em>2</em></p>";
		assertEquals(str, sandra.informe());
	}

}